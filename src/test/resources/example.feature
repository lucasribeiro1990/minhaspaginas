Feature: Unamed

  Scenario: Unexistent
    Given a dummy step
    When I take a dummy action
    Then I get a dummy result
  
  Scenario: Make gitlab generate HTML from feature files
    Given a feature files folder
    When I commit this folder to gitlab
    Then gitlab pages will generate an HTML version of it